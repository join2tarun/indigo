#pragma once
#include <cstring>
#include <vector>
class MyString
{
public:
   MyString();
   MyString(char*in);
   MyString(const MyString&obj);
   MyString& operator=(const MyString&obj);
   MyString(MyString&&obj);
   ~MyString();
   char* getData();
   unsigned int getLength();
   static void AllTest();
   static int Default_ctr;
   static int CopyCons_ctr;
   static int Move_ctr;
   static int Desc_ctr;
   static int Param_ctr;
   static int Assign_ctr;

private:
   char*str;

};
int MyString::Default_ctr = 0;
int MyString::CopyCons_ctr = 0;
int MyString::Move_ctr = 0;
int MyString::Desc_ctr = 0;
int MyString::Param_ctr = 0;
int MyString::Assign_ctr = 0;
MyString::MyString()
{
   Default_ctr++;
   cout << endl << "\t\tDefault Cons Called " << Default_ctr <<" Times "<<endl;
   str = nullptr;
   str = new char[1];
   str[0] = '\0';

}
MyString::MyString(char*in)
{
   if (in)
   {
      Param_ctr++;
      cout << endl << "\t\tParam Cons Called" << Param_ctr << " Times " << endl;
      unsigned int length = strlen(in);
      str = new char[length + 1];
      strcpy(str, in);
      str[length] = '\0';
   }
}

MyString::MyString(const MyString&obj)
{
   CopyCons_ctr++;
   cout << endl << "\t\tCopy Cons Called " << CopyCons_ctr<<" Times "<< endl;
   unsigned int length = strlen(obj.str);
   str = new char[length+1];
   strcpy(str, obj.str);
   str[length] = '\0';
}
MyString& MyString::operator=(const MyString&obj)
{
   if (this!=&obj)
   {
      Assign_ctr++;
      cout << endl << "\t\tAssignment operator Called " << Assign_ctr<<" Times "<< endl;
      delete str;
      unsigned int length = strlen(obj.str);
      str = new char[length + 1];
      strcpy(str, obj.str);
      str[length] = '\0';
   }
   return *this;
}

MyString::MyString(MyString&&obj)
{
   Move_ctr++;
   cout << endl << "\t\tMove Constructor Called " <<Move_ctr<<" Times "<< endl;

   str = nullptr;
   str = obj.str;
   obj.str = nullptr;
}

MyString::~MyString()
{
   Desc_ctr++;
   cout << endl << "\t\tDesc Called " <<Desc_ctr<<" Times "<< endl;
   delete str;
}

char* MyString::getData()
{
   //cout << endl << "getData Called" << endl;
      cout << endl << "Data :";
      return str;

}

unsigned int MyString::getLength()
{
   //cout<< endl << "getLength Called" << endl;
   cout << endl << "Length:";
   if (str)
      return strlen(str);
   else
      return 0;
}
void MyString::AllTest()
{
   {
      MyString mystr;
      cout << mystr.getData() << endl;
      cout << mystr.getLength() << endl;
   }
   {
      MyString mystr1 = "Hello I am Developer";
      cout << mystr1.getData() << endl;
      cout << mystr1.getLength() << endl;
      MyString mystr2 = mystr1;
      cout << mystr2.getData() << endl;
      cout << mystr2.getLength() << endl;

   }
   {
      MyString mystr1 = "Hello I am Developer";
      cout << mystr1.getData() << endl;
      cout << mystr1.getLength() << endl;
      MyString mystr2;
      mystr2 = mystr1;
      cout << mystr2.getData() << endl;
      cout << mystr2.getLength() << endl;

      
   }
   {
      // Create a vector object and add a few elements to it.
      vector<MyString> v;
      v.push_back(MyString("Hello I am Developer"));
      v.push_back(MyString("Hello I am Tester"));

   }
   {
      //test nullptr in param cons
      MyString mystr = nullptr;
      MyString mystr1;// = nullptr;
      mystr1 = "Tarun";
      cout << mystr1.getData() << endl;
      cout << mystr1.getLength() << endl;
   }
   //print counter
   cout << endl << "Default Cons : " << Default_ctr;
   cout << endl << "Param Cons : " << Param_ctr;
   cout << endl << "Copy Cons : " << CopyCons_ctr;
   cout << endl << "Move Cons : " << Move_ctr;
   cout << endl << "Assignment Opr : " << Assign_ctr;
   cout << endl << "Destructor : " << Desc_ctr;

}